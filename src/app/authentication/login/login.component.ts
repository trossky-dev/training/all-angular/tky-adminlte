import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

declare var $;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    document.body.className ="hold-transition login-page";

    $(()=> {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  }

  login(){
    this.router.navigate(['dashboard'])
  }
  ngOnDestroy(): void {
    document.body.className = '';
  }
}
